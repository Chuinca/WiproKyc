package com.wipro.Kyc.repository;

import com.wipro.Kyc.models.KycProfile;
import org.springframework.data.jpa.repository.JpaRepository;


public interface KycRepository extends JpaRepository<KycProfile, Long> {

}