package com.wipro.Kyc.repository;

import com.wipro.Kyc.models.KycProfile;
import com.wipro.Kyc.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * from TBL_USER u where u.USERNAME = :name", nativeQuery = true)
    public User getUserByUsername(@Param("name") String name);

}