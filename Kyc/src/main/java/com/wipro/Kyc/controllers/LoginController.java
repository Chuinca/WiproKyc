package com.wipro.Kyc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class LoginController {


    @GetMapping("/login")
    public String setUsers(Model model){
        return "/login";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute(name = "userName") String username, @ModelAttribute(name = "password") String password, Model model, RedirectAttributes redir){


        if (username.equals("banker") && password.equals("banker123")){
            redir.addAttribute("UNAME",username);
            return "redirect:/home";
        }
        else {
            model.addAttribute("invalidCredentials", true);
            return "login";
        }
    }



}