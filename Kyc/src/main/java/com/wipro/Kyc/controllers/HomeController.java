package com.wipro.Kyc.controllers;

import com.wipro.Kyc.models.KycProfile;
import com.wipro.Kyc.service.KycService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Controller
public class HomeController {


    @Autowired
    private KycService ks;

    private String global;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("/home")
    public String setProfileLists(@RequestParam(value="id", required = false)Long id, Model model, @ModelAttribute("UNAME") String uname){
        List<KycProfile> costumers = new ArrayList<>();

        global=uname;
        if (id != null){
            if (ks.getKycById(id) == null){
                costumers= null;
            }else {
                costumers.add(ks.getKycById(id));
            }
        }
        else {
            costumers= ks.getAllKyc();
        }
        ;
        model.addAttribute("costumers", costumers);
        return "home";
    }

    @ModelAttribute("status")
    public List<Long> kycStatus(ModelMap model) {
        List<Long> statusGreen = new ArrayList<>();

        for (KycProfile profile : ks.getAllKyc()) {
            if (profile.kycCountry != null && profile.occupationType != null && profile.designation != null && profile.salary != null && profile.identificationType != null && profile.identificationNumber != null) {
                statusGreen.add(profile.getId());
            }
        }

        return statusGreen;
    }

    @RequestMapping("/profile")
    public String editProfile(@RequestParam(value = "id", required = false) Long id, Model model){
        if (id != null) {
            model.addAttribute("profile", ks.getKycById(id));
        }

        return "profile/profilePage";
    }

    @PostMapping("/profile")
    public String saveProfile(@RequestParam(value = "id", required = false) Long id,
                              @ModelAttribute KycProfile updatedProfile, Model model,
                              @Valid @ModelAttribute("profile") KycProfile profileCheck,
                              BindingResult br,
                              RedirectAttributes redir){
        String success;
        if (br.hasErrors()){

            return "profile/profilePage";
        }

        if (id == null){
            success="Profile created successfully.";
            redir.addFlashAttribute("successMessage",success);
            ks.createKyc(updatedProfile);
        }else if (id != null){
            success="Profile updated successfully.";
            redir.addFlashAttribute("successMessage",success);
            updatedProfile.setId(id);
            ks.updateKycProfile(updatedProfile);

        }
        model.addAttribute("updatedProfile",updatedProfile);

        return "redirect:KYCInformation?id=" + updatedProfile.getId();
    }

    @RequestMapping("/KYCInformation")
    public String editKycInfo(@RequestParam(value = "id", required = false) Long id, Model model){
        if (id == null) {
            model.addAttribute("noCustomer", "Customer Information is required to save the KYC Information");
            return "profile/kycInfoPage";
        }
        model.addAttribute("profile", ks.getKycById(id));


        return "profile/kycInfoPage";
    }


    @PostMapping("/KYCInformation")
    public String saveKyc(@RequestParam(value = "id", required = false) Long id,
                          @ModelAttribute KycProfile updatedProfile,
                          Model model,
                          @Valid @ModelAttribute("profile") KycProfile checkProfile,
                          BindingResult bs){


        if (bs.hasErrors()) {


            Pattern p = Pattern.compile("[a-zA-Z0-9]{3}-[a-zA-Z0-9]{3}-[a-zA-Z0-9]{4}|[0-9]{7}");
            Matcher m = p.matcher(updatedProfile.getIdentificationNumber());

            if (id == null) {
                model.addAttribute("noCustomer", "Customer Information is required to save the KYC Information");
                return "profile/KYCInfoPage";
            }
            if (bs.hasFieldErrors(updatedProfile.kycCountry) || bs.hasFieldErrors(updatedProfile.identificationType) || bs.hasFieldErrors(updatedProfile.identificationNumber) || bs.hasFieldErrors(updatedProfile.occupationType) || bs.hasFieldErrors(updatedProfile.designation) || bs.hasFieldErrors(updatedProfile.salary)) {
                return "profile/KYCInfoPage";
            } else if ((updatedProfile.getIdentificationType().equals("SSN")  && updatedProfile.getIdentificationNumber().length() != 12) || (updatedProfile.getIdentificationType().equals("Passport")  && updatedProfile.getIdentificationNumber().length() !=7) || (!m.find())){

                model.addAttribute("idNumberError","Invalid Identification Number");
                return "profile/KYCInfoPage";
            }
            if ((!updatedProfile.getKycCountry().equals("USA") && updatedProfile.getIdentificationType().equals("SSN")) || (updatedProfile.getKycCountry().equals("USA") && updatedProfile.getIdentificationType().equals("Passport"))){
                model.addAttribute("idTypeError","Invalid Identification Type");
                return "profile/KYCInfoPage";
            }

            System.out.println(bs.hasFieldErrors(checkProfile.identificationNumber));

        }


        updatedProfile.setId(id);
        ks.updateKycInfo(updatedProfile);

        model.addAttribute("updatedKYCInformation",updatedProfile);

        return "redirect:KYCInformation?id=" + updatedProfile.getId();
    }
}