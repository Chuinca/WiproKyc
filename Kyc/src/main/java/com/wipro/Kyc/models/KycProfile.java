package com.wipro.Kyc.models;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Entity
@Table(name = "TBL_PROFILE")
@Data
public class KycProfile implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Pattern(regexp = "[a-zA-Z]{5,12}", message = "Invalid First Name")
    @NotNull(message = "First Name is required")
    @Column(name = "first_name")
    public String firstName;

    @Pattern(regexp = "[a-zA-Z]{5,12}", message = "Invalid Middle Name")
    @Column(name = "middle_name")
    public String middleName;

    @Pattern(regexp = "[a-zA-Z]{5,12}", message = "Invalid Last Name")
    @NotNull(message = "Last Name is required")
    @Column(name = "last_name")
    public String lastName;

    @NotNull(message = "Gender is required")
    @Column(name = "gender")
    public String gender;

    @Pattern(regexp = "(0[1-9]|1[012])[- .](0[1-9]|[12][0-9]|3[01])[- .](19|20)\\d\\d", message = "Invalid DOB")
    @NotNull(message = "DOB is required")
    @Column(name = "birth_date")
    public String birthDate;

    @Pattern(regexp = ".{5,20}", message = "Invalid Address")
    @NotNull(message = "Address is required")
    @Column(name = "address")
    public String addressLine;

    @Pattern(regexp = "[a-zA-Z]{6,10}", message = "Invalid City")
    @NotNull(message = "City is required")
    @Column(name = "city")
    public String city;

    @Pattern(regexp = "[a-zA-Z]{6,10}", message = "Invalid State")
    @NotNull(message = "State is required")
    @Column(name = "state")
    public String state;

    @Pattern(regexp = "India|USA|Canada", message = "Invalid Country")
    @NotNull(message = "Country is required")
    @Column(name = "country")
    public String country;

    @Pattern(regexp = "[0-9]{6,10}", message = "Invalid Postal Code")
    @Column(name = "postal_code")
    public String postalCode;

    @Pattern(regexp = "India|USA|Canada", message = "Invalid Country")
    @Column(name = "kyc_country")
    public String kycCountry;

    @Pattern(regexp = "Full Time|Part Time", message = "Invalid Occupation Type")
    @Column(name = "occupation_type")
    public String occupationType;

    @Pattern(regexp = "[a-zA-Z]{10,20}", message = "Invalid Designation")
    @Column(name = "designation")
    public String designation;

    @Pattern(regexp = "[0-9]{5,7}", message = "Invalid Salary")
    @Column(name = "salary")
    public String salary;

    @Pattern(regexp = "Passport|SSN", message = "Invalid Country")
    @Column(name = "identification_type")
    public String identificationType;

    @Pattern(regexp = "[a-zA-Z0-9]{3}-[a-zA-Z0-9]{3}-[a-zA-Z0-9]{4}|[0-9]{7}", message = "Invalid Identification Number")
    @Column(name = "identification_number")
    public String identificationNumber;
}
