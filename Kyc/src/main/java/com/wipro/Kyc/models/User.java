package com.wipro.Kyc.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "TBL_USER")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name= "username")
    public String username;

    @Column(name= "password")
    public String password;

}