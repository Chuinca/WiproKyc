package com.wipro.Kyc.service;

import com.wipro.Kyc.models.KycProfile;

import java.util.List;

public interface KycService {
    KycProfile createKyc(KycProfile kycProfile);

    KycProfile updateKycProfile(KycProfile kycProfile);

    KycProfile updateKycInfo(KycProfile kycProfile);

    List<KycProfile> getAllKyc();

    KycProfile getKycById(long KycId);

    void deleteKyc(long id);
}