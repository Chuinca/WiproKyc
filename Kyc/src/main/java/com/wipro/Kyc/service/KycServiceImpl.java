package com.wipro.Kyc.service;

import com.wipro.Kyc.exception.ResourceNotFoundException;
import com.wipro.Kyc.models.KycProfile;
import com.wipro.Kyc.repository.KycRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class KycServiceImpl implements KycService {

    @Autowired
    private KycRepository kycRepository;

    @Override
    public KycProfile createKyc(KycProfile kycProfile) {

        System.out.println("New User created: " + kycProfile.toString());
        return kycRepository.save(kycProfile);

    }

    @Override
    public KycProfile updateKycProfile(KycProfile kycProfile) {
        Optional<KycProfile> kycDb= this.kycRepository.findById(kycProfile.getId());

        if(kycDb.isPresent()) {
            KycProfile kycProfileUpdate = kycDb.get();
            kycProfileUpdate.setId(kycProfile.getId());
            kycProfileUpdate.setFirstName(kycProfile.getFirstName());
            kycProfileUpdate.setMiddleName(kycProfile.getMiddleName());
            kycProfileUpdate.setLastName(kycProfile.getLastName());
            kycProfileUpdate.setGender(kycProfile.getGender());
            kycProfileUpdate.setBirthDate(kycProfile.getBirthDate());
            kycProfileUpdate.setAddressLine(kycProfile.getAddressLine());
            kycProfileUpdate.setCity(kycProfile.getCity());
            kycProfileUpdate.setState(kycProfile.getState());
            kycProfileUpdate.setCountry(kycProfile.getCountry());
            kycProfileUpdate.setPostalCode(kycProfile.getPostalCode());
            kycRepository.save(kycProfileUpdate);
            System.out.println("User updated");
            return kycProfileUpdate;
        }else{
            throw new ResourceNotFoundException("Record not found");
        }
    }

    @Override
    public KycProfile updateKycInfo(KycProfile kycProfile){
        Optional<KycProfile> kycDb= this.kycRepository.findById(kycProfile.getId());

        if(kycDb.isPresent()) {
            KycProfile kycProfileUpdate = kycDb.get();
            kycProfileUpdate.setKycCountry(kycProfile.getKycCountry());
            kycProfileUpdate.setOccupationType(kycProfile.getOccupationType());
            kycProfileUpdate.setDesignation(kycProfile.getDesignation());
            kycProfileUpdate.setSalary(kycProfile.getSalary());
            kycProfileUpdate.setIdentificationType(kycProfile.getIdentificationType());
            kycProfileUpdate.setIdentificationNumber(kycProfile.getIdentificationNumber());
            kycRepository.save(kycProfileUpdate);
            System.out.println("User updated: " + kycProfileUpdate.toString());
            return kycProfileUpdate;
        }else{
            throw new ResourceNotFoundException("Record not found");
        }
    }


    @Override
    public List<KycProfile> getAllKyc() {
        return this.kycRepository.findAll();
    }

    @Override
    public KycProfile getKycById(long kycId) {
        Optional<KycProfile> kycDb= this.kycRepository.findById(kycId);

        if(kycDb.isPresent()) {
            return  kycDb.get();
        }else{
            return null;
        }
    }

    @Override
    public void deleteKyc(long id) {
        Optional<KycProfile> kycDb= this.kycRepository.findById(id);

        if(kycDb.isPresent()) {
            this.kycRepository.delete(kycDb.get());
        }else{
            throw new ResourceNotFoundException("Record not found");
        }
    }
}