package com.wipro.Kyc.service;

import com.wipro.Kyc.models.User;

import java.util.List;

public interface UserService {

    User createUser(User user);

    List<User> getAllUsers();

    User getUserByUsername(String name);

}