DROP TABLE IF EXISTS TBL_USER;
DROP TABLE IF EXISTS TBL_PROFILE;

CREATE TABLE TBL_USER (
    id       INT NOT NULL PRIMARY KEY AUTO_INCREMENT(2),
    username VARCHAR(250) UNIQUE NOT NULL,
    password VARCHAR(250) NOT NULL
);

CREATE TABLE TBL_PROFILE
(
    id         INT NOT NULL PRIMARY KEY AUTO_INCREMENT(100001),
    first_name VARCHAR(250) NOT NULL,
    middle_name VARCHAR(250),
    last_name  VARCHAR(250) NOT NULL,
    gender     VARCHAR(10)  NOT NULL CHECK (gender IN ('Male', 'Female')),
    birth_date VARCHAR(250) NOT NULL,
    address     VARCHAR(250) NOT NULL,
    city       VARCHAR(250) NOT NULL,
    state      VARCHAR(250) NOT NULL,
    country    VARCHAR(10)  NOT NULL CHECK (country IN ('USA', 'India', 'Canada')),
    postal_code VARCHAR(250),
    kyc_country    VARCHAR(10) CHECK (kyc_country IN ('USA', 'India', 'Canada')),
    occupation_type VARCHAR(10) CHECK (occupation_type IN ('Full Time', 'Part Time')),
    designation     VARCHAR(250),
    salary          VARCHAR(250),
    identification_type  VARCHAR(250) CHECK (identification_type IN ('Passport', 'SSN')),
    identification_number VARCHAR(250)
);